# Notes:

This is Visual Studio 2017 Core 2 concept app:

 * Install [.NET Core 2 SDK](https://www.microsoft.com/net/download/windows) first (if you havent already)
 * May work with older VS versions (but this hasn't been tested)

#### To run

* open in VS 2017
* let the environment download dependencies
* hit F5 or Start button to run the app
* you should see three windows open up (one with server API and two different clinets consuming it)

#### Illustrates few coding principles

* classes, with exception of interfaces, are composed
* server API and both clients are shells into which desired functionality is injected
* polymorphism is illustated by different types of readers and writes implementing the same interface
* IOC, with exception of tests and CLI client (comments in code), used throughout
* interfaces crafted for specific use cases (minimal)
* both client(s) and server API can take any readers/writers as long as they implement specified interface,
* services mocked during testing (where it made sense)
* more ...

#### Caveats
* error handling, when employed, uses 
```c#
try 
{
	// do stuff
}
catch(Exeption ex)
{
	// handle error in some way
}
```
which catches all the errors and that's not what should be done normally but in this case I didn't want to have to implement every error handles for every possible exception that can be thrown when reading/writing to files or communicating with remote servers
* not every testing scenario achieves 100% coverage nor is comprehensive; generally speaking you should always test input parameters for existence and validity and handle any errors that used server(s) or your own code may throw
* these classes always return contracted values: so, if a server is supposed to return a string then even in case of an error (in it's own code) it 1) handles its own errors and 2) returns and empty string leaving the interpretation of it's meaning up to the caller (client)   
* DbWriter, for obvious reasons, does not include any implementation and is included in the solution for illustrative purposes only, however, implementation can easily follow example illustrated in FileWriter, but using your favorite DB along with a client for it