﻿using Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private IReader _reader;

        public ValuesController(IReader reader)
        {
            _reader = reader;
        }

        // GET api/values
        [HttpGet]
        public string Get()
        {
            // reader will return string even if there was an exception
            return _reader.Read();
        }
    }
}
