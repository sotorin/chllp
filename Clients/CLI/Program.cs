﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            // read config settins for the RestReader
            IConfiguration builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            // there is too much plumbing to set up dependency injection
            // for this client so to simplify things we use 'new' operator
            // normally IOC container would be used just like in web client
            var serviceResponse = new RestReader.RestReader(builder["Service:Url"], new System.Net.Http.HttpClient()).Read();
            new DbWriter.DbWriter("connection string").Write(serviceResponse);

            Console.WriteLine(serviceResponse);
            Console.Read();
        }
    }
}
