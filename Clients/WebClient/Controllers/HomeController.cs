﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebClient.Models;
using Interfaces;

namespace WebClient.Controllers
{
    public class HomeController : Controller
    {
        private IReader _reader;
        private IWriter _writer;

        public HomeController(IReader reader, IWriter writer)
        {
            _reader = reader;
            _writer = writer;
        }
        public IActionResult Index()
        {
            var read = _reader.Read();
            _writer.Write(read);
            ViewData["Data"] = read;

            return View();
        }
    }
}
