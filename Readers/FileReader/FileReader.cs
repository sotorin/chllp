﻿using Interfaces;
using System;
using System.IO;

namespace FileReader
{
    public class FileReader : IReader, IDisposable
    {
        private string _pathToFile;

        public FileReader(string pathToFile)
        {
            _pathToFile = pathToFile;
        }

        public void Dispose() { }

        public string Read()
        {
            // whole bunch of things can go wrong here so try/catch is in order
            // error handling was not part of requirement thus I'm cutting corners
            // and catching all exceptions, which is not approprate practice
            // plus of course our file contains only one line, "Hello World"
            try
            {
                using (var reader = new StreamReader(_pathToFile))
                {
                    var line = reader.ReadLine();
                    return line == "Hello World" ? line : string.Empty;
                }
            }
            catch (Exception)
            {
                // handle exceptions any way you see fit, I'll just return an empty string
                return string.Empty;
            }
        }
    }
}
