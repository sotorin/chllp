﻿using Interfaces;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace RestReader
{
    public class RestReader : IReader, IDisposable
    {
        private string _url;
        private HttpClient _client;

        public RestReader(string url, HttpClient client)
        {
            _url = url;
            _client = client;
        }
        public void Dispose() { }

        public string Read()
        {
            if (string.IsNullOrEmpty(_url) || _client == null) return string.Empty;

            try
            {
                using (_client)
                {
                    _client.DefaultRequestHeaders.Accept.Clear();
                    var response = _client.GetAsync(_url).Result; // this is blocking call, but OK
                    using (HttpContent content = response.Content)
                    {
                        Task<string> result = content.ReadAsStringAsync();
                        return result.Result;
                    }
                }
            }
            // should only catch exceptions we know how to handle
            // again, this is not approach that should be taken for error handling
            // but I'm doing it here just to speed things up...
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
