﻿using Interfaces;
using System;

namespace DbWriter
{
    public class DbWriter : IWriter, IDisposable
    {
        private string _conn;

        public DbWriter(string conn)
        {
            _conn = conn;
        }
        public void Dispose() { }

        public bool Write(string text)
        {
            // implement Db access accordingly to DB type, used clients, etc.
            // I'm not going to do it here because it's trivial.
            // let's just presume that the data was saved to DB.
            return true;
        }
    }
}
