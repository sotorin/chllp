﻿using Interfaces;
using System;
using System.IO;

namespace FileWriter
{
    public class FileWriter : IWriter, IDisposable
    {
        private string _pathToFile;

        public FileWriter(string pathToFile)
        {
            _pathToFile = pathToFile;
        }

        public void Dispose() { }

        public bool Write(string text)
        {
            if (string.IsNullOrEmpty(_pathToFile) || string.IsNullOrEmpty(text)) return false;
            // lots of things can go wrong when writing to file
            try
            {
                if (File.Exists(_pathToFile))
                {
                    using (var file = new StreamWriter(_pathToFile))
                    {
                        file.WriteLine("Written to on: " + DateTime.Now + " content: " + text);
                    }
                    return true;
                }
                
                return false;
            } // don't do it like this, I'm just cutting corners.
            catch (Exception)
            {
                // handle error here: we'll just return
                return false;
            }
        }
    }
}
