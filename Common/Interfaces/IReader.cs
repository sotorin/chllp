﻿namespace Interfaces
{
    public interface IReader
    {
        string Read();
    }
}
