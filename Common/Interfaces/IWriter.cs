﻿namespace Interfaces
{
    public interface IWriter
    {
        bool Write(string text);
    }
}
