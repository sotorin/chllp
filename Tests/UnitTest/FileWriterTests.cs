﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class FileWriterTests
    {
        [TestMethod]
        public void Return_False_If_Path_Or_Text_Is_Null()
        {
            Assert.AreEqual(false, new FileWriter.FileWriter(null).Write("Some text"));
            Assert.AreEqual(false, new FileWriter.FileWriter("path").Write(null));
        }

        [TestMethod]
        public void Return_False_If_Path_Points_To_Non_Existing_File()
        {
            Assert.AreEqual(false, new FileWriter.FileWriter("somePathHere").Write("Some text"));
        }

        [TestMethod]
        public void Return_True_If_Existing_File_Specified()
        {
            var file = Directory.GetCurrentDirectory() + @"\" + "filetowriteto.txt";
            if (CreateFile(file))
            {
                Assert.AreEqual(true, new FileWriter.FileWriter(file).Write("Some text"));
                DeleteFile(file);
            }
            else Assert.Fail();
        }

        private bool CreateFile(string pathToFile)
        {
            try
            {
                if (File.Exists(pathToFile))
                {
                    File.Delete(pathToFile);
                }

                using(StreamWriter sw = File.CreateText(pathToFile))
                {
                    sw.Write("");
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void DeleteFile(string pathToFile)
        {
            if (File.Exists(pathToFile)) File.Delete(pathToFile);
        }
    }
}
