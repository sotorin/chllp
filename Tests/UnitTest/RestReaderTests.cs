﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RichardSzalay.MockHttp;
using System.Net.Http;

namespace UnitTest
{
    [TestClass]
    public class RestReaderTests
    {
        [TestMethod]
        public void Return_Empty_String_If_Invalid_Url_Passed_In()
        {
            var invalidUrl = "http://someinvalidurl.com/values";
            var mockHttpClinet = new MockHttpMessageHandler();
            mockHttpClinet.When(invalidUrl).Respond("application/text", string.Empty);

            var client = new HttpClient(mockHttpClinet);

            Assert.AreEqual(string.Empty, new RestReader.RestReader(invalidUrl, client).Read());
        }

        [TestMethod]
        public void Return_Hello_World_If_Valid_Url_Passed_In()
        {
            var validUrl = "http://localhost:63763/api/values";
            var mockHttpClinet = new MockHttpMessageHandler();
            mockHttpClinet.When(validUrl).Respond("application/text", "Hello World");

            var client = new HttpClient(mockHttpClinet);

            Assert.AreEqual("Hello World", new RestReader.RestReader(validUrl, client).Read());
        }

        [TestMethod]
        public void Return_Empty_String_If_Url_Is_Null_Or_Empty()
        {
            var validUrl = "http://localhost:63763/api/values";
            var mockHttpClinet = new MockHttpMessageHandler();
            mockHttpClinet.When(validUrl).Respond("application/text", "Hello World");

            var client = new HttpClient(mockHttpClinet);

            Assert.AreEqual(string.Empty, new RestReader.RestReader(null, client).Read());
        }

        [TestMethod]
        public void Return_Empty_String_If_Clinet_Is_Null()
        {
            Assert.AreEqual(string.Empty, new RestReader.RestReader("someURL", null).Read());
        }
    }
}
