using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class FileReaderTests
    {
        private const string FILE_NAME = "sample.txt";
        private const string CONTENT = "Hello World";

        [TestMethod]
        public void Returns_An_Empty_String_On_Exception()
        {
            Assert.AreEqual(string.Empty, new FileReader.FileReader("some fake path").Read());
        }

        [TestMethod]
        public void Return_Hello_World_If_Valid_Content_Provided()
        {
            try
            {
                var path = Directory.GetCurrentDirectory() + @"\" + FILE_NAME;
                if (CreateFile(path, CONTENT))
                {
                    Assert.AreEqual(CONTENT, new FileReader.FileReader(path).Read());
                    DeleteFile(path);
                }
                else Assert.Fail();
            }
            catch (Exception)
            {
                Assert.Fail();
            }  
        }

        [TestMethod]
        public void Return_Empty_String_If_File_Contained_Line_Other_Than_Hello_World()
        {
            try
            {
                var path = Directory.GetCurrentDirectory() + @"\" + FILE_NAME;
                if (CreateFile(path, "Some wrong content"))
                {
                    Assert.AreEqual(string.Empty, new FileReader.FileReader(path).Read());
                    DeleteFile(path);
                }
                else Assert.Fail();

            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        private bool CreateFile(string pathToFile, string text)
        {
            try
            {
                if (File.Exists(pathToFile))
                {
                    File.Delete(pathToFile);
                }

                using (StreamWriter sw = File.CreateText(pathToFile))
                {
                    sw.WriteLine(text);
                }
       
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void DeleteFile(string pathToFile)
        {
            if (File.Exists(pathToFile)) File.Delete(pathToFile);
        }
    }
}
